var photoBlocks = $('.photo-block');

$(document).ready(function () {
    $.stellar({
        horizontalScrolling: false,
        responsive: true
    });

    var photoColorArray = [
        './app/images/1.png',
        './app/images/2.png',
        './app/images/3.png',
        './app/images/4.png',
        './app/images/5.png',
        './app/images/6.png',
        './app/images/7.png',
        './app/images/8.png',
        './app/images/9.png'
    ];

    var photoNotColorArray = [
        './app/images/photo1.png',
        './app/images/photo2.png',
        './app/images/photo3.png',
        './app/images/photo4.png',
        './app/images/photo5.png',
        './app/images/photo6.png',
        './app/images/photo7.png',
        './app/images/photo8.png',
        './app/images/photo9.png'
    ];

    function bgImageBlock () {
        for (var i = 0; i < photoBlocks.length; i++) {
            $(photoBlocks[i]).css("background-image", "url('" + photoNotColorArray[i] + "')");
            var slide = $(photoBlocks[i]).find('.text-block');
            $(slide).slideUp();
        }
    }
    bgImageBlock();

    $(photoBlocks).mouseenter(function () {
        var bgImage = './app/' + $(this).css('background-image').split('"')[1].split('app/')[1];

        for(var i = 0; i < photoColorArray.length; i++) {
            if (bgImage === photoNotColorArray[i]) {
                console.log(bgImage, photoNotColorArray[i])
                $(this).css('background-image', "url(" + photoColorArray[i] + ")");
                $(this).find('.text-block').slideDown();
            }
        }
    });

    $(photoBlocks).mouseleave(function () {
        bgImageBlock();
    });

    $('.burger-menu').click(function () {
        $('.menu-mobile').css('display', 'block');
        $('.close-icon').css('display', 'block');
    });

    $('.close-icon').click(function () {
        $('.menu-mobile').css('display', 'none');
        $('.close-icon').css('display', 'none');
    });

    $('.js-card').on('show.bs.collapse', function() {
        $(this).find('.down').replaceWith('<img class="up" src="./app/icons/up.png">');
        //$(this).find('.card-header').css('border', 'none');
    });
    $('.js-card').on('hide.bs.collapse', function() {
        $(this).find('.up').replaceWith('<img class="down" src="./app/icons/down.png">');
        //$(this).find('.card-header').css('border-bottom', '1px solid rgb(190,190,190)');
    });

    $('#up').click(function() {
        $('html, body').animate({scrollTop: 0},500);
        return false;
    });


    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 120,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    });

    if (window.matchMedia('(max-width: 600px)').matches) {
        swiper = new Swiper('.swiper-container', {
            slidesPerView: 1,
            spaceBetween: 10,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            }
        });
    } else if (window.matchMedia('(max-width: 768px)').matches)
    {
        swiper = new Swiper('.swiper-container', {
            slidesPerView: 2,
            spaceBetween: 50,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            }
        });
    }

    $(function(){
        $('a[href*=#]').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
                && location.hostname == this.hostname) {
                var $target = $(this.hash);
                $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
                if ($target.length) {
                    var targetOffset = $target.offset().top - ($(".header-block").outerHeight(true)); //#main-header - заменить на ваш элемент
                    $('html,body').animate({scrollTop: targetOffset}, 500);
                    $('.menu-mobile').css('display', 'none');
                    $('.close-icon').css('display', 'none');
                    return false;
                }
            }
        });
    });
});