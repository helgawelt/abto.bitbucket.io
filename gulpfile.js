var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var surge = require('gulp-surge')

var paths = {
    sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass']);

gulp.task('sass', function(done) {
    gulp.src('./scss/styles.app.scss')
        .pipe(sass())
        .on('error', sass.logError)
        //.pipe(gulp.dest('./www/css/'))
        .pipe(rename( 'app.css'))
        .pipe(gulp.dest('./app/'))
        .on('end', done);
});

gulp.task('watch', ['sass'], function() {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('deploy', [], function () {
    return surge({
        project: '../abto.bitbucket.io',         // Path to your static build directory
        domain: 'abto_new.surge.sh'  // Your domain or Surge subdomain
    })
});